:sectnums:
:toc: auto
= Ansible OpenShift NFS Persistent Volume

== 개요

이 프로젝트는 Ansible 을 이용해, OpenShift Container Platform 이 사용할 NFS Persistent Volume 을 생성하는 예제입니다. 

== 파일 

=== Ansible 인벤토리 파일

sample_hosts 파일은 Ansible 을 실행할 때 변수 정보와 서버 주소 정보를 포함하는 Ansible 인벤토리 파일입니다. 
아래 값들을 Ansible 실행 전 수정합니다.

[cols="1,3,2",options="header"]
|=======
|항목 |설명 | 예
|ansible_user |자동화 계정 변수 | ec2-user
|bastion |Bastion (oc 실행 가능) 서버 주소  | support1.be91.internal
|nfs |NFS 서버 주소 | support1.be91.internal
|=======

.sample-hosts
[source, INI]
----
[all:vars]
###########################################################################
### Ansible Vars
###########################################################################
timeout=60
ansible_user=ec2-user
ansible_become=yes

[bnastion]
support1.be91.internal

[nfs]
support1.be91.internal
----

=== Ansible 실행 변수

variales.yml 파일은 Ansible 을 실행할 때 참조하는 변수를 정의합니다. 
아래 값들을 Ansible 실행 전 수정합니다.

[cols="1,4,2",options="header"]
|=======
|항목 |설명 | 예
|nfs_server |NFS 서버 주소 | support1.be91.internal
|ocp_server| OpenShift 서버 주소 |master1.be91.internal 
|ocp_server_port| OpenShift 서버 접속 포트 |443 
|ocp_token |OpenShift Cluster 
 Persistent Volume 을 생성할 권한을 갖는 사용자 계정의 로그인 토큰 | RXe...

|pv_names | 생성할 Persistent Volume 이름 나열| - "pv0001"
|=======

* oc_token 은 다음 명령을 이용해 획득합니다. 

[source, bash]
----
oc login https://master.xxx -u admin -p xxxx
oc whoami -t
RXecd3hUZ711RjaCmCHWWaEV6Qaza77sbP45uYL-Glg
----

CAUTION: 위 명령에서 admin 은 OpenShift Persistent Volume 을 생성할 권한을 갖는 사용자 계정입니다.

.variables.yml
[source, yaml]
----
nfs_server: support1.be91.internal 
ocp_server: master1.be91.internal
ocp_server_port: 443
ocp_token: FXKqfJgJiSoro8FvEfIHkON-5LiP8BiyHd6Ae30krnE
pv_names: 
  - "pv0001"
  - "pv0002"   
  - "pv0003"   
  - "pv0004"   
  - "pv0005"   
  - "pv0006"   
  - "pv0007"   
  - "pv0008"   
  - "pv0009"   
  - "pv0010"     
----

=== NFS 서버 플레이북

nfs-dir-create.yml 은 nfs 디렉토리를 생성하는 Ansible 플레이북입니다. 

* /exports/ 디렉토리에 OpenShift Persistent Volume 을 위한 NFS 디렉토리를 생성합니다. 
* 플레이북 내 firewalld 변경 태스크들은 의도적으로 실행되지 않게 태그를 부착했습니다. (필요한 경우 해제)
 
=== OpenShift Persistent Volume 플레이북

pv-create.yml 은 OpenShift Persistent Volume 을 생성하는 Ansible 플레이북입니다.

* NFS 서버 /exports/ 디렉토리에 생성된 디렉토리가 Persistent Volume 디렉토리로 사용됩니다. 
* 100Gi 크기 Volume 을 생성합니다. (필요시 수정)
* RWO, RWX 모드를 모두 사용합니다. (필요시 수정)
* 자원 회수 정책은 Recycle 을 사용합니다. (필요시 Retain 으로 수정) 

CAUTION: 현재 oc 모듈을 이용했는데, 이 모듈은 Ansible 실행 시, deprecate 되어 openshift_raw 로 대체하라는 경고가 발생합니다. (deprecate 경고는 ansible.cfg 에서 꺼놓았습니다.) 

== Ansible 실행

=== 소스 복제 

git lab 에서 nfs-ocp-ansible-playbook 소스를 복제합니다. 
[source, bash]
----
git clone https://gitlab.com/hinunbi/nfs-ocp-ansible-playbook.git
----

=== NFS 서버

Ansible 명령을 실행하기 전, 다음 조건이 만족돼야 합니다.

* NFS 서비스를 위한 RHEL/CentOS 서버 준비
* yum 설치 또는 sudo 로 root 전환 가능한 계정 (RHEL) 
* 실행(bastion) 서버에 Ansible 설치 

다음 명령을 실행해 NFS 서버에 OpenShioft Persistent Volume 을 위한 디렉토리를 생성합니다.  

[source, bash]
----
cd nfs-ocp-ansible-playbook
ansible-playbook --extra-vars=@variables.yml -i sample-hosts nfs-dir-create.yml 
----

=== OpenShift Persistent Volume

Ansible 명령을 실행하기 전, 다음 조건이 만족돼야 합니다.

* NFS 서버 자동화 실행 완료
* 실행 중인 OpenShift Container Plaltform 클러스터 환경
* OpenShift Persistent Volume 생성 권한을 갖는 계정 (예, cluster admin)
* 실행(bastion) 서버에 Ansible 설치 

CAUTION:  이 예제는 OCP master 서버 포트로 443 을 사용했습니다. 실무적으로 8443 을 많이 사용하므로 Ansible 을 실행하기 전 variables.yml 에 ocp_server_port 설정에 유의해 주십시오. 

다음 명령을 실행해 NFS 서버와 OpenShift Persistent Volume 을 생성합니다. 
[source, bash]
----
cd nfs-ocp-ansible-playbook
ansible-playbook --extra-vars=@variables.yml -i sample-hosts pv-create.yml 
----

